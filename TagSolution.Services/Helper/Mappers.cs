﻿using System.Linq;
using TagSolution.Entities.Entities;
using TagSolution.Services.Models;

namespace TagSolution.Services.Helper
{
    public static class Mappers
    {
        public static TagModel TagtoTagModel(this Tag tag)
        {
            return new TagModel
            {
                Format = tag.TagFormat,
                Id = tag.Id,
                ImpressionPixels = tag.ImpressionPixels.Select(x=> x.ImpressionPixelToImpressionPixelModel()).ToList(),
                Name = tag.Name,
                Method = tag.ServingMethod,
                Type = tag.BannerType
            };
        }

        public static Tag TagModelToTag(this TagModel tagModel)
        {
            return new Tag
            {
                Id = tagModel.Id,
                ServingMethod = tagModel.Method,
                BannerType = tagModel.Type,
                Name = tagModel.Name,
                TagFormat = tagModel.Format
            };
        }

        public static ImpressionPixelModel ImpressionPixelToImpressionPixelModel(this ImpressionPixel impressionPixel)
        {
            return new ImpressionPixelModel
            {
                Id = impressionPixel.Id,
                Pixel = impressionPixel.Pixel
            };
        }

        public static ImpressionPixel ImpressionPixelModelToImpressionPixel(
            this ImpressionPixelModel impressionPixelModel)
        {
            return new ImpressionPixel
            {
                Id =  impressionPixelModel.Id,
                Pixel = impressionPixelModel.Pixel
            };
        }
    }
}