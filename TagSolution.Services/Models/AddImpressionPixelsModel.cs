﻿using System;
using System.Collections.Generic;

namespace TagSolution.Services.Models
{
    public class AddImpressionPixelsModel
    {
        public Guid TagId { get; set; }
        public List<string> Pixels { get; set; }
    }
}