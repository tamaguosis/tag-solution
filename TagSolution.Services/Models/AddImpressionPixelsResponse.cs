﻿using System;
using System.Collections.Generic;

namespace TagSolution.Services.Models
{
    public class AddImpressionPixelsResponse: ResultModel<List<ImpressionPixelModel>>
    {
        
    }

    public class ImpressionPixelModel {
    public Guid Id { get; set; }
    public string Pixel { get; set; }
    }
}