﻿using System.Collections.Generic;

namespace TagSolution.Services.Models
{
    public class ResultModel<T>
    {
        public ResultModel()
        {
            Success = true;
        }

        public bool Success { get; set; }

        public T Data { get; set; }

        public List<string> Messages { get; set; }
    }
}