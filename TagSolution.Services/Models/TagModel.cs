﻿using System;
using System.Collections.Generic;
using TagSolution.Entities.Enums;

namespace TagSolution.Services.Models
{
    public class TagModel
    {
        public TagModel()
        {
            ImpressionPixels = new List<ImpressionPixelModel>();
        }
        public Guid Id { get; set; }
        public string Name { get; set; }
        public BannerType Type { get; set; }
        public ServingMethod Method { get; set; }
        public TagFormat Format { get; set; }
        public List<ImpressionPixelModel> ImpressionPixels { get; set; }
    }
}