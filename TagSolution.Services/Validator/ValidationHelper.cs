﻿using System.Collections.Generic;
using System.Linq;
using TagSolution.Entities.Entities;
using TagSolution.Entities.Enums;

namespace TagSolution.Services.Validator
{
    public static class ValidationHelper
    {
        private static readonly IDictionary<BannerType, ValidationModel> rules = new Dictionary<BannerType, ValidationModel>
        {
            { BannerType.Html, new ValidationModel(new List<ServingMethod>{ServingMethod.Click, ServingMethod.Impressions}, new List<TagFormat>{TagFormat.Iframe, TagFormat.JavascriptDefault}, true )},
            { BannerType.Image,  new ValidationModel(new List<ServingMethod>{ServingMethod.Impressions, ServingMethod.Click}, new List<TagFormat>{TagFormat.JavascriptDefault, TagFormat.Iframe}, true )},
            { BannerType.Keyword,new ValidationModel(new List<ServingMethod>{ServingMethod.Click}, new List<TagFormat>{TagFormat.JavascriptDefault, TagFormat.Iframe} )},
            { BannerType.Video, new ValidationModel(new List<ServingMethod>{ServingMethod.Xml}, new List<TagFormat>{TagFormat.UrlToXml}, true)}
        };

        public static ValidationResponseModel ValidateTag(this Tag tag)
        {
            var response = new ValidationResponseModel();
            rules.TryGetValue(tag.BannerType, out var typeRules);
            if (typeRules != null)
            {
                if (!typeRules.AllowedServingMethods.Contains(tag.ServingMethod))
                {
                    response.Success = false;
                    response.Messages.Add("Serving Method is not allowed");
                }

                if (!typeRules.AllowedFormats.Contains(tag.TagFormat))
                {
                    response.Success = false;
                    response.Messages.Add("Tag Format is not allowed");
                }

                if (!typeRules.ImpressionPixelsAllowed && tag.ImpressionPixels.Any())
                {
                    response.Success = false;
                    response.Messages.Add("Impression Pixels are not allowed");
                }
            }

            return response;
        }
    }
}