﻿using System.Collections.Generic;
using TagSolution.Entities.Enums;

namespace TagSolution.Services.Validator
{
    public class ValidationModel
    {
        public ValidationModel(List<ServingMethod> servingMethods, List<TagFormat> tagFormats, bool allowedPixels = false)
        {
            AllowedServingMethods = servingMethods;
            AllowedFormats = tagFormats;
            ImpressionPixelsAllowed = allowedPixels;
        }
        public bool ImpressionPixelsAllowed { get; set; }
        public List<ServingMethod> AllowedServingMethods { get; set; }
        public List<TagFormat> AllowedFormats { get; set; }
    }
}