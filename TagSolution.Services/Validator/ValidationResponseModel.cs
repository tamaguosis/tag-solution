﻿using System.Collections.Generic;

namespace TagSolution.Services.Validator
{
    public class ValidationResponseModel
    {
        public ValidationResponseModel()
        {
            Success = true;
            Messages = new List<string>();
        }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
    }
}