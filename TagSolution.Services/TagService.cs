﻿using System;
using System.Collections.Generic;
using System.Linq;
using TagSolution.Entities.Entities;
using TagSolution.Repository.TagRepo;
using TagSolution.Services.Helper;
using TagSolution.Services.Models;
using TagSolution.Services.Validator;

namespace TagSolution.Services
{
    public class TagService : ITagService
    {
        private readonly ITagRepository _repository;

        public TagService(ITagRepository repository)
        {
            _repository = repository;
        }

        public GetTagListResponse GetTagList()
        {
            var response = new GetTagListResponse();

            var query = "SELECT * FROM Tags";
            response.Data = _repository.Query<Tag>(query).Select(x => x.TagtoTagModel()).ToList();

            return response;
        }

        public TagResponse GetTagById(Guid id)
        {
            var query = "SELECT * FROM tags t LEFT JOIN ImpressionPixels ip on ip.TagId = t.Id WHERE t.Id = @tagId";
            var response = new TagResponse();
            var tagDictionary = new Dictionary<Guid, Tag>();

            response.Data = _repository.Query<Tag, ImpressionPixel>(query, (tag, impressionPixel) =>
            {
                if (!tagDictionary.TryGetValue(tag.Id, out var tagEntry))
                {
                    tagEntry = tag;
                    tagDictionary.Add(tagEntry.Id, tagEntry);
                }

                if (impressionPixel != null)
                {
                    tagEntry.ImpressionPixels.Add(impressionPixel);
                }
                return tagEntry;

            }, new { tagId = id }).Select(x => x.TagtoTagModel()
            ).FirstOrDefault();

            return response;
        }

        public TagResponse AddTag(TagModel tagObj)
        {
            var tag = tagObj.TagModelToTag();

            var validationResult = tag.ValidateTag();
            var response = new TagResponse { Success = validationResult.Success, Messages = validationResult.Messages };

            var query = "INSERT INTO Tags (id, bannerType, tagFormat, servingMethod) Values (@id, @bannerType, @tagFormat, @servingMethod)";

            _repository.Execute(query, tag);

            if (response.Success)
            {
                response.Data = tag.TagtoTagModel();
            }

            return response;
        }

        public TagResponse EditTag(TagModel tagObj)
        {
            var tag = tagObj.TagModelToTag();

            var validationResult = tag.ValidateTag();
            var response = new TagResponse { Success = validationResult.Success, Messages = validationResult.Messages };

            var query =
                "UPDATE Tags SET BannerType = @bannerType, TagFormat = @tagFormat, ServingMEthod = @servingMethod WHERE id = @id";

            _repository.Execute(query, tag);

            if (response.Success)
            {
                response.Data = tag.TagtoTagModel();
            }

            return response;
        }

        public AddImpressionPixelsResponse AddImpressionPixels(AddImpressionPixelsModel model)
        {
            var response = new AddImpressionPixelsResponse();

            var query = "SELECT * FROM Tags WHERE id = @tagId";
            var tag = _repository.Query<Tag>(query, new { tagId = model.TagId }).FirstOrDefault();

            if (tag != null)
            {
                model.Pixels.ForEach(pixel =>
                {
                    tag.ImpressionPixels.Add(new ImpressionPixel
                    {
                        Id = Guid.NewGuid(),
                        TagId = tag.Id,
                        Pixel = pixel
                    });
                });

                var validationResult = tag.ValidateTag();
                response.Success = validationResult.Success;
                response.Messages = validationResult.Messages;

                if (validationResult.Success)
                {

                    query = "INSERT INTO ImpressionPixels (id, tagId, pixel) Values (@id, @tagId, @pixel)";

                    _repository.Execute(query, tag.ImpressionPixels);
                    response.Data = tag.ImpressionPixels.Select(a => a.ImpressionPixelToImpressionPixelModel())
                        .ToList();
                }
            }
            else
            {
                response.Success = false;
            }

            return response;
        }

        public DeleteResponse DeleteImpressionPixel(Guid id)
        {
            var query = "DELETE FROM ImpressionPixels WHERE id = @id";

            var result = _repository.Execute(query, new { id });

            return new DeleteResponse { Success = result > 0 };
        }
    }
}
