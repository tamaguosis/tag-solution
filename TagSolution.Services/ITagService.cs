﻿using System;
using TagSolution.Services.Models;

namespace TagSolution.Services
{
    public interface ITagService
    {
        GetTagListResponse GetTagList();
        TagResponse GetTagById(Guid id);
        TagResponse AddTag(TagModel tagObj);
        TagResponse EditTag(TagModel tagObj);
        AddImpressionPixelsResponse AddImpressionPixels(AddImpressionPixelsModel model);
        DeleteResponse DeleteImpressionPixel(Guid id);
    }
}