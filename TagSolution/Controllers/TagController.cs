﻿using System;
using Microsoft.AspNetCore.Mvc;
using TagSolution.Services;
using TagSolution.Services.Models;

namespace TagSolution.API.Controllers
{
    [Route("api/tag")]
    public class TagController : BaseController
    {
        private readonly ITagService _service;

        public TagController(ITagService service)
        {
            _service = service;
        }

        [HttpGet("getTagList")]
        public GetTagListResponse GetTags()
        {
            return Execute(() => _service.GetTagList());
        }

        [HttpGet("getTag/{id}")]
        public TagResponse GetTag(Guid id)
        {
            return Execute(() => _service.GetTagById(id));
        }

        [HttpPost("addTag")]
        public TagResponse AddTag([FromBody]TagModel model)
        {
            return Execute(() => _service.AddTag(model));
        }

        [HttpPut("editTag")]
        public TagResponse EditTag([FromBody]TagModel model)
        {
            return Execute(() => _service.EditTag(model));
        }

        [HttpPost("addPixels")]
        public AddImpressionPixelsResponse AddImpressionPixels([FromBody] AddImpressionPixelsModel model)
        {
            return Execute(() => _service.AddImpressionPixels(model));
        }

        [HttpDelete("deletePixel/{id}")]
        public DeleteResponse DeleteImpressionPixel(Guid Id)
        {
            return Execute(() => _service.DeleteImpressionPixel(Id));
        }
    }
}
