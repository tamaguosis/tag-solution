﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace TagSolution.API.Controllers
{
    public abstract class BaseController : Controller
    {
        public T Execute<T>(Func<T> func)
        {
            try
            {
                return func();
            }
            catch
            {
                throw new Exception("Internal Server Error");
            }
        }
    }
}