﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TagSolution.Entities.Entities
{
    public class EntityBase
    {
        [Key]
        public Guid Id { get; set; }
    }
}