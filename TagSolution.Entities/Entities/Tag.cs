﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TagSolution.Entities.Enums;

namespace TagSolution.Entities.Entities
{
    [Table("Tags")]
    public class Tag : EntityBase
    {
        public Tag()
        {
            ImpressionPixels = new List<ImpressionPixel>();
        }

        public string Name { get; set; }
        public BannerType BannerType { get; set; }
        public TagFormat TagFormat { get; set; }
        public ServingMethod ServingMethod { get; set; }
        public List<ImpressionPixel> ImpressionPixels { get; set; }
    }
}