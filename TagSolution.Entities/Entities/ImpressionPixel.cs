﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TagSolution.Entities.Entities
{
    [Table("ImpressionPixels")]
    public class ImpressionPixel: EntityBase
    {
        public string Pixel { get; set; }

        public Guid TagId { get; set; }
    }
}