﻿namespace TagSolution.Entities.Enums
{
    public enum BannerType
    {
        Image,
        Video,
        Html,
        Keyword
    }
}
