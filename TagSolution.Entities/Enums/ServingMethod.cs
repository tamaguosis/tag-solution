﻿namespace TagSolution.Entities.Enums
{
    public enum ServingMethod
    {
        Impressions,
        Click,
        Xml
    }
}