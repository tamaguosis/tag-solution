﻿namespace TagSolution.Entities.Enums
{
    public enum TagFormat
    {
        JavascriptDefault,
        Iframe,
        UrlToXml
    }
}