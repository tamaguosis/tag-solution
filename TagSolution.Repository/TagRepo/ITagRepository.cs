﻿using System;
using System.Collections.Generic;
using TagSolution.Entities.Entities;

namespace TagSolution.Repository.TagRepo
{
    public interface ITagRepository
    {
        IEnumerable<T> Query<T>(string sql, object parameters = null) where T: EntityBase;
        IEnumerable<T1> Query<T1, T2>(string sql, Func<T1, T2, T1> mapFunc, object parameters = null) where T1 : EntityBase where T2 : EntityBase;
        int Execute(string sql, object parameters);
    }
}