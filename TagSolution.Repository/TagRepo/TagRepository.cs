﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Options;
using TagSolution.Entities;
using TagSolution.Entities.Entities;

namespace TagSolution.Repository.TagRepo
{
    public class TagRepository : ITagRepository
    {
        private readonly IOptions<DataSettings> _dataSettings;

        public TagRepository()
        {
            
        }

        public TagRepository(IOptions<DataSettings> dataSettings)
        {
            this._dataSettings = dataSettings;
        }

        internal IDbConnection Connection => new SqlConnection(_dataSettings.Value.ConnectionString);

        public IEnumerable<T> Query<T>(string sql, object parameters = null) where T : EntityBase
        {
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                return cn.Query<T>(sql, parameters);
            }
        }

        public IEnumerable<T1> Query<T1, T2>(string sql, Func<T1, T2, T1> mapFunc, object parameters = null) where T1 : EntityBase where T2 : EntityBase
        {
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                return cn.Query<T1, T2, T1>(sql, mapFunc, parameters);
            }
        }

        public int Execute(string sql, object parameters)
        {
            using (IDbConnection cn = Connection)
            {
                cn.Open();
                return cn.Execute(sql, parameters);
            }
        }
    }
}