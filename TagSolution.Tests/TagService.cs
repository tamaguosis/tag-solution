using System;
using System.Collections.Generic;
using Moq;
using TagSolution.Entities.Entities;
using TagSolution.Entities.Enums;
using TagSolution.Repository.TagRepo;
using TagSolution.Services;
using TagSolution.Services.Models;
using Xunit;

namespace TagSolution.Tests
{
    public class TagServiceTests
    {
        private ITagService service;
        private Mock<ITagRepository> repository;

        public TagServiceTests()
        {
            repository = new Mock<ITagRepository>();

            service = new Services.TagService(repository.Object);
        }

        [Fact]
        public void Should_Call_GetTagList_Which_Returns_List_Of_Tags()
        {
            repository.Setup(x => x.Query<Tag>(It.IsAny<string>(), null)).Returns(new List<Tag> { new Tag() });

            var result = service.GetTagList();

            Assert.Single(result.Data);

            var query = "SELECT * FROM Tags";
            repository.Verify(x => x.Query<Tag>(query, null), Times.Once());
        }

        [Fact]
        public void Should_Return_Tag()
        {
            repository.Setup(x => x.Query(It.IsAny<string>(), It.IsAny<Func<Tag, ImpressionPixel, Tag>>(), It.IsAny<object>()))
                .Returns(new List<Tag> { new Tag(), new Tag() });

            var id = Guid.NewGuid();
            var result = service.GetTagById(id);

            Assert.NotNull(result.Data);
        }

        [Fact]
        public void AddTag_Should_Return_Validation_Errors()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>()));

            var result = service.AddTag(new TagModel
            {
                Format = TagFormat.UrlToXml,// invalid format
                Type = BannerType.Image
            });

            Assert.False(result.Success);
            Assert.Contains("Tag Format is not allowed", result.Messages);
        }

        [Fact]
        public void AddTag_Should_Return_Tag_When_Added()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>()));

            var result = service.AddTag(new TagModel
            {
                Format = TagFormat.Iframe,
                Type = BannerType.Image
            });

            Assert.True(result.Success);
            Assert.NotNull(result.Data);
        }

        [Fact]
        public void EditTag_Should_Return_Validation_Errors()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>()));

            var result = service.EditTag(new TagModel
            {
                Format = TagFormat.UrlToXml,// invalid format
                Type = BannerType.Image
            });

            Assert.False(result.Success);
            Assert.Contains("Tag Format is not allowed", result.Messages);
        }

        [Fact]
        public void EditTag_Should_Return_Tag_When_Added()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>()));

            var result = service.EditTag(new TagModel
            {
                Format = TagFormat.Iframe,
                Type = BannerType.Image
            });

            Assert.True(result.Success);
            Assert.NotNull(result.Data);
        }

        [Fact]
        public void AddImpressionPixels_Should_Return_Error_Tag_Type_Is_Not_Valid()
        {
            repository.Setup(x => x.Query<Tag>(It.IsAny<string>(), It.IsAny<object>())).Returns(new List<Tag> { new Tag{ BannerType = BannerType.Keyword } });

            var result =
                service.AddImpressionPixels(new AddImpressionPixelsModel
                {
                    Pixels = new List<string> {"test"},
                    TagId = Guid.NewGuid()
                });

            Assert.False(result.Success);
            Assert.Contains("Impression Pixels are not allowed", result.Messages);
        }

        [Fact]
        public void AddImpressionPixels_Should_Return_Added_Pixels()
        {
            repository.Setup(x => x.Query<Tag>(It.IsAny<string>(), It.IsAny<object>())).Returns(new List<Tag> { new Tag { BannerType = BannerType.Html } });

            var result =
                service.AddImpressionPixels(new AddImpressionPixelsModel
                {
                    Pixels = new List<string> { "test" },
                    TagId = Guid.NewGuid()
                });

            Assert.True(result.Success);
            Assert.Contains(result.Data, x =>x.Pixel == "test");
        }

        [Fact]
        public void DeleteImpressionPixel_Should_Return_Success_False()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>())).Returns(0);

            var result = service.DeleteImpressionPixel(Guid.NewGuid());

            Assert.False(result.Success); // because 0 rows was updated
        }

        [Fact]
        public void DeleteImpressionPixel_Should_Return_Success_True()
        {
            repository.Setup(x => x.Execute(It.IsAny<string>(), It.IsAny<object>())).Returns(1);

            var result = service.DeleteImpressionPixel(Guid.NewGuid());

            Assert.True(result.Success); 
        }
    }
}
